# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView
from django.template import Context, loader

from Bio import Entrez, Medline
from datetime import datetime

import numpy as np
import matplotlib.pyplot as plot
from matplotlib.ticker import MaxNLocator

class HomePageView( TemplateView ):

    database = "nucleotide"
    publicationDate = "pdat"
    maxResults = 10000

    yearTo = ""
    yearFrom = ""
    query = ""

    def printInputs( self, request ):
        print( request.POST.get( 'input', None ) )
        print( request.POST.get( 'from', None ) )
        print( request.POST.get( 'to', None ) )
    
    def get( self, request, **kwargs ):
        return render( request, 'index.html', context=None )

    def queryEntrez( self ):
        yearTo = int( self.yearTo )
        yearFrom = int( self.yearFrom )
        occurrences = { str( yearFrom ) : 0 }
        curr = yearFrom + 1
        while curr <= yearTo:
            occurrences[ str( curr ) ] = 0
            curr = curr + 1

        Entrez.email = "rowan.joice@hotmail.co.uk"
        handle = Entrez.esearch(
            db = 'pubmed',
            retmax = self.maxResults,
            term = self.query,
            datetype = self.publicationDate,
            mindate = self.yearFrom,
            maxdate = self.yearTo )
        
        identifiers = Entrez.read( handle )[ 'IdList' ]

        handle = Entrez.efetch(
            db = 'pubmed',
            id = identifiers,
            retmax = self.maxResults,
            rettype = "medline",
            retmode = "text" )
        
        record = Medline.parse( handle )
        year = ""
        
        for i in record:
            year = datetime.strptime( i["DP"][:4], '%Y' ).date().year
            if not ( ( year > yearTo ) or ( year < yearFrom ) ):
                occurrences[ str( year ) ] = occurrences[ str( year ) ] + 1

        total = 0
        for year, i in occurrences.items():
            total = total + i
        print( "total = " + str( total ) )
        return str( total ) + " results found", occurrences

    def makeChart( self, occurrences ):
        for keys, values in occurrences.items():
            print( keys + " : " + str( values ) )

        currYear = int( self.yearFrom )
        xAxisLabels = [""] * len( occurrences )
        barValues = [0] * len( occurrences )
        pos = 0

        while currYear <= int( self.yearTo ):
            print( pos )
            xAxisLabels[ pos ] = str( currYear )
            barValues[ pos ] = occurrences.get( str( currYear ) )
            pos = pos + 1
            currYear = currYear + 1

        print( xAxisLabels )
        print( barValues )

        index = np.arange( len( occurrences ) )
        barWidth = 0.35
        figure, axis = plot.subplots()

        rects = axis.bar( index, barValues, barWidth, color='b' ) 

        axis.set_xlabel( 'Year of Article' )
        axis.set_ylabel( 'Number of Articles Published' )
        axis.set_title( 'Number of Articles Published Year by Year Mentioning ' + self.query )
        axis.set_xticks( index )
        axis.set_xticklabels( xAxisLabels )

        figure.tight_layout()
        #plot.show()
        figure.savefig( 'static/plot.png' )

        return
    
    def post( self, request, **kwargs ):
        self.printInputs( request )
        
        self.query = request.POST.get( 'input', None )
        self.yearFrom = request.POST.get( 'from', None )
        self.yearTo = request.POST.get( 'to', None )

        if self.query == "" or self.yearFrom == "" or self.yearTo == "":
            context = {
                'query' : self.query,
                'yearFrom' : self.yearFrom,
                'yearTo' : self.yearTo ,
                'dateFault' : "false" }
            template = loader.get_template( 'error.html' )
            return HttpResponse( template.render( context ) )
        elif self.yearFrom > self.yearTo:
            context = {
                'query' : self.query,
                'yearFrom' : self.yearFrom,
                'yearTo' : self.yearTo ,
                'dateFault' : "true" }
            template = loader.get_template( 'error.html' )
            return HttpResponse( template.render( context ) )
        else:
            total, results = self.queryEntrez()
            self.makeChart( results )
            ctx = { 'matches' : total, 'stats' : '<img src="plot.png" alt="plot">' }
            return render( request, 'responseDisease.html', context = { 'matches' : total } )
    
    

class ErrorView( TemplateView ):
    def get( self, request, **kwargs ):
        return render( request, 'error.html', context=None)
